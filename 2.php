<?php

/**
 * crear array
 */

/** opcion 1 **/
$vocales=['a','e','i','o','u'];

/** opcion 2 **/
$vocales=array('a','e','i','o','u');

/** opcion 3 **/
$vocales=[];
$vocales[]='a';
$vocales[]='e';
$vocales[]='i';
$vocales[]='o';
$vocales[]='u';

/** opcion 4 **/
$vocales=[
    0 => 'a',
    1 => 'e',
    2 => 'i',
    3 => 'o',
    4 => 'u'
];

var_dump($vocales); 


