<?php

$datos=[
    [
        "nombre" => "Eva",
        "edad" => 50
    ],
    [
        "nombre" => "Jose",
        "edad" => 40,
        "peso" => 80
    ]
];

// añadir el registro
// Lorena de altura 175
// realizarlo directamente


// opcion 1
$datos[2]["nombre"]="Lorena";
$datos[2]["altura"]=175;

// opcion 2
$datos[]=[
 "nombre" => "Lorena"   ,
 "altura" => 175   
];


// introducir dos registros
// Luis de 20 años y peso 90
// Oscar de 23 años
// utilizar push

// sin array_push
$datos[]=[
    "nombre" => "Luis",
    "edad" => 20,
    "peso" => 90
];

$datos[]=[
    "nombre" => "Oscar",
    "edad" => 23
];

// con array_push

array_push($datos,
        [
            "nombre" => "Luis",
            "edad" => 20,
            "peso" => 90 
        ],
        [
            "nombre" => "Oscar",
            "edad" => 23    
        ]
    );

var_dump($datos);