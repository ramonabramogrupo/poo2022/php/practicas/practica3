<?php

// array asociativo
$numeros=[
    "cero" => 0,
    "uno" => 1,
    "dos" => 2,
    "tres" => 3,
    "cuatro" => 4,
];

// array enumerado
$vocales=['a','e','i','o','u'];


// leer el segundo elemento del array numeros y mostrarlo

echo $numeros["uno"];
echo "<br>";

// leer el segundo elemento del array vocales y mostrarlo
echo $vocales[1];       
echo "<br>";

// añadir un elemento al final de vocales con la a con tilde

/* opcion 1 */
$vocales[5]='á';

/* opcion 2 */
$vocales[]='á';

/* opcion 3 */
array_push($vocales,'á');

// añadir un elemento nuevo al array numeros con indice cinco con el valor 5

$numeros['cinco']=5;

var_dump($vocales,$numeros);
        
        
        



