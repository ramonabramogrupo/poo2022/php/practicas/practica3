<?php

$vocales=['a','e','i','o','u'];

// mostrar la longitud del array con una funcion
echo count($vocales);
echo "<br>";

// mostrar una vocal aleatorio utilizando mt_rand
$indice=mt_rand(0,4); // devuelve numero aleatorio entre 0 y 4

echo $vocales[$indice];

// mostrar una vocal aleatorio utilizando mt_rand
$indice=mt_rand(0,4);
echo $vocales[$indice];

// opcion 2
// puedo colocar la funcion dentro de la lectura
echo $vocales[mt_rand(0,4)];